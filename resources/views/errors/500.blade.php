<!--<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Error 500</title>
</head>
<body>
    <p>Error interno del servido</p>
</body>
</html>-->
@extends('layouts.plantilla-errores',
    ['titulo' => '500 INTERNAL SERVER ERROR', 'error'=>'500', 'css'=>'500', 'cols'=>'4', 'rows'=>'3'])
@section('contenidoError')
<p>Ups...Esto es culpa nuestra, inténtalo más tarde a ver si ya se ha arreglado.</p>
@endsection
@section('puzzle')
<div class="puzzle1"><img src="{{asset ('img/gutemberg.jpg')}}"></div>
<div class="puzzle2"><img src="{{asset ('img/gutemberg.jpg')}}"></div>
<div class="puzzle3"><img src="{{asset ('img/gutemberg.jpg')}}"></div>
<div class="puzzle4"><img src="{{asset ('img/gutemberg.jpg')}}"></div>


@endsection