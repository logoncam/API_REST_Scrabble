<div class="modal fade" tabindex="-1" id="areaUsuarios" data-backdrop="static">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="titulos">Acceso</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body caja-formulario w-100">
        <p id="mensajeLogin"></p>
        <form id="loginForm">
            <div class="form-group">
                <label class="titulos" for="nombre">Email </label>
                <input type="email" class="form-control " id="email" placeholder="Correo electrónico">
            </div>
            <div class="form-group">
                <label class="titulos" for="pw">Contraseña </label>
                <input type="password" class="form-control" id="pw">
            </div>
            <button type="submit" class="btn btn-dark">Acceder</button>
        </form>
       <p> <a href="{{ route('password.request') }}" title="contraseña olvidada">¿Has olvidado tu contraseña? </a></p>
        <p><a href="{{ route('register') }}" title="contraseña olvidada">Registro </a></p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>