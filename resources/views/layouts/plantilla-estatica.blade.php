<!DOCTYPE html>
<html lang="es">
<x-header titulo="{{$titulo}}" css="forms"/>
<!-- no deberia ser en el body porque no todos tienen menu lateral //no funciona en chrome!!-->
<body data-spy="scroll" data-target="#menuLateral" >
    <div class="container"> 
        <x-navegacion/>
        <div class="row justify-content-center" id="contenidoP">
             <x-aside size="3" sticky="sticky-top">
                <x-slot name="menus">
                     @yield('menus')
                </x-slot>
            </x-aside>
             <div class="col-lg-9 p-4" role="main" aria-label="{{$titulo}}">
             @yield('contenido')
             </div>
        </div>
         <x-pie/>
    </div>
     <script src="{{ asset('/js/app.js') }}"></script>
</body>
</html>