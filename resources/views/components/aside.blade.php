@props(['menus','size','sticky','reversed'])

<div class=" position-relative col-lg-{{$size}}  d-none d-lg-block" role="complementary" aria-label="menu lateral">
  <div class="{{$sticky ?? ' '}} pt-4">
  @if (isset($reversed) && $reversed)
        {{$menus}} 
        <x-news-full/>
  @else
          <x-news-full/>
        <!--Aqui la navegacion de las secciones de la misma pag.
          asi que hay q coger todas las subsecciones y hacer una lista o algo-->
          {{$menus}} 
  @endif
  </div>
</div>
<!-- sticky-top-->
<div class="col-lg d-lg-none d-block sticky-top barra--news" role="complementary" aria-label="noticias">
       <x-news-slim/>
</div>