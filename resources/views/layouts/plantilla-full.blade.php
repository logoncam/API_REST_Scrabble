<!DOCTYPE html>
<html lang="es">
     <!--arreglar con un slot css q no se puedan pasar arrays?-->
<x-header titulo="{{$titulo}}" css="{{$css ?? 'forms' }}"/>
<body>
<div class="container-fluid w-var"> 
        <x-navegacion/>
        <div class="row justify-content-center ">
             <x-aside reversed="{{$reversed ?? false}}" size="2">
                <x-slot name="menus">
                     @yield('menus')      
                </x-slot>
            </x-aside>

             <div class="col  p-4 order-lg-1 order-2" role="main" aria-label="{{$titulo}}">
                 @yield('contenido')
             </div>
             <!--mostrar solo si se dice que si?-->
             @if ($columnas === '3')
             <div class="col-lg-3 p-4 bloque-stats-scrabble order-lg-2 order-1" role="complementary" aria-label="Informacion juego">
                @yield('aside-derecha')
             </div>
             @endif
        </div>
         <x-pie/>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    @if (isset($js) && is_array($js) )
        @foreach ($js as $filejs)
        <script  src="{{ asset('js/'.$filejs.'.js')}}">   </script>
        @endforeach
     @endif
    
</body>
</html>