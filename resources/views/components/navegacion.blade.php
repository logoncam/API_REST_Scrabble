<nav class="navbar row navbar-expand-lg navbar-light bg-light">
    <h1 class="logo"><a id="top" href="/">La imprenta</a></h1>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menuDesplegable" aria-controls="menuDesplegable" aria-expanded="false" aria-label="Expandir menu">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="sr-only sr-only-focusable" href="#contenidoP">Ir al contenido principal</a>
    <div class="collapse navbar-collapse justify-content-end" id="menuDesplegable">
      <ul class="navbar-nav">
        <!--hay q hacer que cambie el active...-->
        <li class="nav-item">
          <a class="nav-link" href="/">Inicio </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/gutenberg">Gutenberg</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/imprenta-en-espana">La imprenta en España</a>
          </li>
        <li class="nav-item">
          <a class="nav-link" href="/scrabble">
           Scrabble
          </a>
         @auth
         <li class="nav-item">
          <a class="nav-link" href="/scrabble/dashboard" ><i class="fa fa-user fa-lg"></i> <span class="d-lg-none">Acceso </span></a>
        </li>
        @endauth
        @guest
        <li class="nav-item">
          <a class="nav-link" href="#" data-toggle="modal" data-target="#areaUsuarios"><i class="fa fa-user fa-lg"></i> <span class="d-lg-none">Acceso </span></a>
        </li>
        @endguest
       
      </ul>
    </div>
  </nav>
  @guest
  <x-login-modal/>
  @endguest