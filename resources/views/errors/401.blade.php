@extends('layouts.plantilla-errores',
    ['titulo' => '401 UNAUTHORIZED', 'error'=>'401', 'css'=>'401'])
@section('contenidoError')
<p>No estás autorizado para acceder a esta página. ¿Porque no vuelves al <a href="/" title="inicio">incio</a>?</p>
@endsection

