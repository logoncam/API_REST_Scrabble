@extends('layouts.plantilla-estatica',
    ['titulo' => 'La imprenta en España'])
    @section('menus')
<div class="list-group" id="menuLateral" role="navigation" aria-label="Contenidos">
    <a href="#libros" class="list-group-item list-group-item-action active">Primeros Libros en España</a>
    <a href="#lugares" class="list-group-item list-group-item-action">Lugares Emblemáticos</a>
</div>
@endsection
    @section('contenido')
    <h4 id="libros" class="titulos">Los Primeros Libros en España</h4>
    <p>El primer libro impreso en España fue el El sinodal de Aguilafuente en 1472 en Segovia.</p>
    <p>Por su parte, los tres primeros impresos en València con el procedimiento de Gutenberg fueron:</p>
    <div class="row justify-content-center mb-4">
        <div class="col-12 col-lg-4 card text-center pt-4" >
         <a href="#trobes" data-toggle="modal"  data-target="#trobes">   
        <img  class="img-cuadrada img-position-center no-cover rounded-circle card-img-top " src="{{asset('img/trobes.jpg')}}" alt="Obres o trobes en laors de la Verge Maria">
        </a>
             <div class="card-body">
                 <h5 class="titulos">Obres o trobes en laors de la Verge Maria</h5>
                    <p>1474</p>
            </div>
        </div>
        <div class=" col-12 col-lg-4 pt-4 card text-center" >
        <a href="#comprehensorium" data-toggle="modal"  data-target="#comprehensorium">
        <img  class="img-cuadrada img-position-left no-cover rounded-circle card-img-top " src="{{asset('img/comprehensorium.jpg')}}" alt="Comprehensorium">
        </a>
             <div class="card-body">
                 <h5 class="titulos">Comprehensorium</h5>
                    <p>1475</p>
            </div>
        </div>
        <div class="col-12 col-lg-4 pt-4 card text-center" >
        <a href="#biblia-valenciana" data-toggle="modal"  data-target="#biblia-valenciana">
        <img  class="img-cuadrada  img-position-center no-cover rounded-circle card-img-top" src="{{asset('img/bibliavalenciana.jpg')}}" alt="Biblia valenciana">
        </a>
             <div class="card-body">
                 <h5 class="titulos">Biblia valenciana</h5>
                    <p>1478</p>
            </div>
        </div>
    </div>
  <!--MODALES-->


  <div class="modal" id ="biblia-valenciana" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <img  class="w-100" src="{{asset('img/bibliavalenciana.jpg')}}" alt="Biblia valenciana">
            </div>
        </div>
    </div>
</div>
<div class="modal " id ="trobes" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <img class="w-100" src="{{asset('img/trobes.jpg')}}" alt="Obres o trobes en laors de la Verge Maria">
      </div>
    </div>
  </div>
</div>

<div class="modal " id ="comprehensorium" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <img class="w-100" src="{{asset('img/comprehensorium.jpg')}}" alt="Comprehensorium">
      </div>
    </div>
  </div>
</div>

    <!-- INTINERARIO!? VALENCIA --->
    <h4 id ="lugares" class="titulos">Lugares emblemáticos de la imprenta valenciana </h4>
    <p>Lugares emblemáticos de las primeras imprentas en València con el procedimiento de Gutenberg.</p>
    <!--masonry?-->
    <div class="card-columns " id="acordeon">
    <div class="card card-size-variable">
      <img class="card-img" src="{{asset('img/portavaldigna.jpg')}}" alt="Card image" data-toggle="collapse" data-target="#valdigna">
      <div id="valdigna" class="collapse" data-parent="#acordeon">
        <div class="card-body" >
        <h5 class="titulos">Imprenta de Palmart</h5>
        <p class="card-text">Junto al Portal de la Valladigna se situaron los talleres de imprenta de donde salieron:“Trobes en loors de la Verge María” “Comprehensorium” “Biblia valenciana”.</p>
        </div>
      </div>
    </div>
    <div class="card card-size-variable">
      <img class="card-img" src="{{asset('img/portalvaldigna2.jpg')}}" alt="Portal de La valdigna calles" data-toggle="collapse" data-target="#valdigna">
    </div>
    <div class="card card-size-variable">
      <img class="card-img" src="{{asset('img/monasteriopuig.jpg')}}" alt="Card image" data-toggle="collapse" data-target="#puig">
      <div id="puig" class="collapse" data-parent="#acordeon">
        <div class="card-body" >
            <h5 class="titulos">El Monasterio de Santa María del Puig</h5>
            <p class="card-text">Alberga el Museo de la Imprenta y contiene una réplica exacta de la imprenta utilizadaGutenberg y que se conserva en Maguncia (Alemania).</p>
        </div>
      </div>
    </div>
    <div class="card card-size-variable">
      <img class="card-img" src="{{asset('img/sanvicentepatriciomey.png')}}" alt="Patricio Mey" data-toggle="collapse" data-target="#patriciomey">
    </div>
    <div class="card card-size-variable">
      <img class="card-img" src="{{asset('img/sanvicentepatriciomey2.jpg')}}" alt="Patricio mey" data-toggle="collapse" data-target="#patriciomey">
      <div id="patriciomey" class="collapse" data-parent="#acordeon">
      <div class="card-body" >
             <h5 class="titulos">Imprenta de Patricio Mey</h5>
            <p class="card-text">En el número 3 de la calle San Vicente se imprimió la segunda edición de “Don Quijote de laMancha”.</p>
        </div>
    </div>
    </div>
    <div class="card card-size-variable">
      <img class="card-img " src="{{asset('img/mercadocentral.jpg')}}" alt="Mercado Central" data-toggle="collapse" data-target="#mercadocentral">
      <div id="mercadocentral" class="collapse" data-parent="#acordeon">
      <div class="card-body" >
             <h5 class="titulos">El Molí de Rovella</h5>
            <p class="card-text">En la confluencia de las actuales calles Barón de Cárcer y Pie de la Cruz estuvo ubicada laprimera imprenta en València.</p>
        </div>
    </div>
    </div>
</div>
  

@endsection