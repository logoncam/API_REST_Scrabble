@extends('layouts.plantilla-estatica',
    ['titulo' => 'La imprenta'])

 @section('menus')
<div class="list-group" id="menuLateral" role="navigation" aria-label="contenido">
    <a href="#como" class="list-group-item list-group-item-action active">¿Cómo se Inventó?</a>
    <a href="#trabajos" class="list-group-item list-group-item-action">Los trabajos de una imprenta</a>
    <a href="#difusion" class="list-group-item list-group-item-action">Difusión de la idea</a>
</div>
@endsection
@section('contenido')
    <span  id="como" ></span>
    <h4 class="titulos">¿Cómo se inventó?</h4>
    <!-- homogeneizar imagenes-->
    <div class="row justify-content-between">
            <div class=" col-12 col-lg-8">
                <p>El nombre de Gutenberg lo asociamos a la invención de la imprenta, pero mucho antes que él ya seimprimía sobre pergamino o papel. Un breve recorrido histórico nos indica que:</p>
                <ul class="fa-ul">
                    <li class="mb-4"> <span class="fa-li"><i class="fas fa-language"> </i></span>2000 años antes de Gutenberg, en Roma se imprimían carteles con signos grabados enarcilla.</li>
                    <li class="mb-4">  <span class="fa-li"><i class="fas fa-language"> </i></span>1400 años antes de Gutenberg, en China se imprimían carteles utilizando signos grabados enmadera. Así en el año 686 se imprimió una publicación que se llamó “El sutra del diamante”,con signos grabados en una única madera</li>
                </ul>

            </div>
            <div class="  col-12 col-lg-3">
              <!--centrar-->
                  <img class="img-cuadrada img-position-90 img-square-rounded no-cover" src="{{asset('img/sutra.png')}}" alt="Sutra"  data-toggle="modal"  data-target="#sutra">
                  <img class="img-cuadrada img-position-90 img-square-rounded e no-cover img-position-center" src="{{asset('img/tiposchinos.jpg')}}" alt="Ejemplo de tipos chinos" data-toggle="modal"  data-target="#tipos-chinos">

            </div>
          </div>
          <!--CENTRAR EN MOVIL-->
    <div class="row justify-content-center align-items-center">
        <div class=" col-12 col-lg-4  p-4 border-time-derecha time-text">
          <p>Pero los moldes de madera tenían un problema: pronto se desgastaban y se echaban a perder. Lagran idea de Gutenberg fue moldear piezas de metal para cada letra, creando de tipos de letras demetal individuales para la imprenta</p>
        </div>
        <div class="col-12 col-lg-4 p-4">
          <img class="img-cuadrada img-position-90 img-square-rounded" src="{{asset('img/fundidor.png')}}" alt="Fundidor" data-toggle="modal"  data-target="#fundidor">
        </div>
    </div>
    <div class="row justify-content-center align-items-center">
        <div class="col-12 col-lg-4  order-2 order-lg-1 p-4 border-time-derecha">
        <img class="img-cuadrada img-position-90 img-square-rounded" src="{{asset('img/tiposmetalmoviles.jpg')}}" alt="tipos moviles de metal" data-toggle="modal"  data-target="#tipos-metal">
        <img class="img-cuadrada img-position-90 img-square-rounded" src="{{asset('img/tiposmoviles.jpeg')}}" alt="Tipos Móviles" data-toggle="modal"  data-target="#tipos-moviles">

        </div>
        <div class="col-12 col-lg-4 order-1 order-lg-2 p-4">
           <p>Después sólo quedaba componer en una cajita, letra a letra, el texto que se quería imprimir, cajitaque se manchaba con unos tampones entintados.</p>
        </div>
    </div>
    <div class="row justify-content-center align-items-center">
        <div class="col-12 col-lg-4 p-4 border-time-derecha">
           <p>Finalmente, sobre las letras metálicas entintadas se colocaba el papel y se presionaba con un aparato así:</p>  
        </div>
        <div class="col-12 col-lg-4 p-4">
           <img class="img-cuadrada img-position-90 img-square-rounded" src="{{asset('img/prensagutenberg.png')}}" alt="prensa de guttemberg" data-toggle="modal"  data-target="#prensa">
        </div>
    </div>
      <div class="row justify-content-center mt-4">
        <p>En resumen, Gutenberg confeccionó un abecedario con letras y signos de plomo. Su idea fueeficaz porque la perfeccionó con:</p>
        <ul class="fa-ul ">
            <li><span class="fa-li"><i class="far fa-thumbs-up"></i></span>Letras Móviles</li>
            <li> <span class="fa-li"><i class="far fa-thumbs-up"></i></span>Molde de metal</li>
            <li> <span class="fa-li"><i class="far fa-thumbs-up"></i></span>Fundidor de tipos o aparato de fundición manual</li>
            <li> <span class="fa-li"><i class="far fa-thumbs-up"></i></span>Aleación especial de metales para fabricar los móviles (plomo, antimonio y bismuto)</li>
            <li> <span class="fa-li"><i class="far fa-thumbs-up"></i></span>Prensa de madera anclada al suelo y al techo</li>
            <li> <span class="fa-li"><i class="far fa-thumbs-up"></i></span>Tinta de imprimir en un determinado papel</li>
        </ul>
      </div>
      <!--modales de las imagenes de la imprenta-->
      <x-modal-img src="{{asset('img/sutra.png')}}" alt="sutra" id="sutra"/>
      <x-modal-img src="{{asset('img/tiposchinos.jpg')}}" alt="tipos chinos" id="tipos-chinos"/>
      <x-modal-img src="{{asset('img/fundidor.png')}}" alt="fundidor" id="fundidor"/>
      <x-modal-img src="{{asset('img/tiposmetalmoviles.jpg')}}" alt="tipos moviles metal" id="tipos-metal"/>
      <x-modal-img src="{{asset('img/tiposmoviles.jpeg')}}" alt="tipos moviles" id="tipos-moviles"/>
      <x-modal-img src="{{asset('img/prensagutenberg.png')}}" alt="prensa gutenberg" id="prensa"/>

    <span  id="trabajos" ></span>
    <h4 class="titulos">Los trabajos de una imprenta en del siglo XVI</h4>
    <p>Por eso, en una imprenta se necesitaban fundamentalmente 3 personas:</p>
    <!--PROGRESO, IMG HOMOGENEAS, MODAL CON FULL IMG AL HACER CLIC-->
    <div class="row mb-4" id="acordeon">
    <div class="col" >
        <div class="card">
          <div class="card-header">
            <h5 class="titulos" data-toggle="collapse" data-target=".componedor">
            <i class="fas fa-angle-double-down"></i> El componedor
            </h5>
          </div>
          <div class=" componedor collapse"  data-parent="#acordeon">
            <div class="card-body row">
                <div class="col-12 col-lg-4 text-center">
            <img class="card-img img-cropped" id="cropped-trabajador-1" src="{{asset('img/trabajadores1.png')}}" alt="imagen componedor">
                </div>
                <div class="col-12 col-lg-8 p-md-5 p-4">
                <p >Realiza el trabajo más delicado. A medida que lee el manuscrito coloca en una cajita, una auna, todas las piezas de metal con letras y espacios que forman una línea. Debe hacerlo enorden inverso. Y cajita a cajita, confecciona toda una página.</p>   
                </div>
            </div>
          </div>
        </div>
        <div class="card">
            <div class="card-header">
              <h5  class="titulos" data-toggle="collapse" data-target=".entintador">
              <i class="fas fa-angle-double-down"></i> El entintador
              </h5>
            </div>  
            <div class="collapse entintador"  data-parent="#acordeon">
            <div class="card-body row">
                <div class="col-12 col-lg-4 text-center">
              <img class="card-img-top img-cropped" id="cropped-trabajador-2" src="{{asset('img/trabajadoresmarcados2.jpg')}}" alt="Imagen entintador"> 
                </div>
                <div class="col-12 col-lg-8 p-md-5 p-4">
                <p >Encargado de entintar la superficie de letras que ha elaborado el componedor. Par ello utilizados tampones semiesféricos impregnados de tinta, uno en cada mano.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
              <h5 class="titulos" data-toggle="collapse" data-target=".tirador">
              <i class="fas fa-angle-double-down"></i> El tirador
              </h5>
            </div>  
            <div id="tirador" class="collapse tirador"  data-parent="#acordeon">
            <div class="card-body row">
                <div class="col-12 col-lg-4 text-center">
              <img class="card-img-top img-cropped " id="cropped-trabajador-3" src="{{asset('img/trabajadoresimprenta3.jpg')}}" alt="imagen tirador">
                </div>
                <div class="col-12 col-lg-8 p-md-5 p-4 ">
                <p >Coloca papel sobre la superficie de letras entintadas y acciona la palanca que hace bajar laprensa sobre los tipos metálicos que colocó el componedor, de manera que quedanmarcadas en el papel</p>
                </div>
              </div>
            </div>
      </div>
</div>
</div>
<div class="video-fondo mb-4">
        <div class="superpuesto"></div>
        <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
          <source src="{{asset('video/imprenta-proceso-fin.mp4')}}" type="video/mp4">
        </video>
        <div class="container h-100">
          <div class="d-flex h-100 text-center align-items-center">
            <div class="w-100 text-white">
              <h4 class="titulos text-white">Impresión con prensa</h4>
              <p>El proceso entero <button type="button" data-toggle="modal" data-target="#playListImprenta" class="btn btn-dark">aquí</button></p>
            </div>
          </div>
        </div>
    </div>
<!--modal video playlist-->
<div class="modal" tabindex="-1" id="playListImprenta">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">El proceso de la impresión con prensa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="embed-responsive embed-responsive-16by9">
          <iframe src="https://www.youtube.com/embed/M2SanMKYdKk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
    

    <h4 class="titulos">Difusión de la idea</h4>
    <p>La idea de Gutenberg de las letras individuales grabadas en metal se difunde por EuropaComienza en MAINZ (hacia 1450)</p>
    <p>Van apareciendo las principales ciudades en donde se instalan imprentas:</p>
    <div class="row" id="mapaeuropa">
    <!--mapa?, si, el de abajo xD-->
        <div class="col-12 col-lg-6">
        <ul class="list-group">
            <li class="list-group-item" id="1469"> 1469 Venecia (Italia)</li>
            <li  class="list-group-item" id="1470">1470 París (Francia)</li>
            <li class="list-group-item" id="1471">1471 Brujas (Holanda)</li>
            <li class="list-group-item" id="1472">1472 Segovia (España)</li>
            <li class="list-group-item" id="1474">1474 Valencia (España)</li>
            <li class="list-group-item" id="1476">1476 Westminster (Gran Bretaña)</li>
        </ul>
        </div>
        <div class=" col-12 col-lg-6 mt-4">
            <div role="img" aria-labelledby="#mapaeuropa" class="img-mapa img-europa img-grises">
            </div>
        </div>
</div>

    <span id="difusion"></span>
    
@endsection
