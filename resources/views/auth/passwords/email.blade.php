@extends('layouts.plantilla-estatica',
    ['titulo' => 'Recuperación de Contraseña'])
@section('contenido')
    <div class="rejilla">
        <h3 class="titulos">Solicitud de recuperación de Contraseña</h3>
        <div class="caja-formulario">
                 @if ($errors->has('email'))
                    <p>{{ $errors->first('email') }}</p>
                @endif
                @if (session('status'))
                     <p>{{ session('status') }}</p>
                @endif
            <form method="POST" action="{{ route('password.email') }}">
                @csrf <!-- por razones educativas está desactivado -->

                <label class="titulos" for="email">Correo electrónico</label>
                <input id="email" type="email" name="email" class="{{ $errors->has('email') ? 'error' : '' }}" required>
                <input class="btn btn-dark full" type="submit" value="Enviar solicitud de reinicio">
             </form>
        </div>    
    </div>
  
@endsection
