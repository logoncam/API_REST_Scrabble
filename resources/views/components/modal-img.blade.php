@props(['src','alt','id'])
<!--ya mete el solo el role="dialog"-->
<div class="modal fade" id="{{$id}}" tabindex="-1" aria-label="imagen {{$alt}}" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <img  class="w-100" src="{{$src}}" alt={{$alt}}>
      </div>
      
    </div>
  </div>
</div>