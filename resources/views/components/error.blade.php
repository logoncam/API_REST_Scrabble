<!--aqui con cssgrid-->
@props(['error', 'titulo','contentError','puzzle'])
<div class="container" role="main" aria-label="error">
    <h2 class="titulos" id="title">{{$titulo}}</h2>
    <div class="img-error-{{$error}}" aria-labbelledby="#title">
        
        {{$puzzle ?? ' '}}
    </div>
    <div class="content-error">
       {{$contentError}}
    </div>
</div>

