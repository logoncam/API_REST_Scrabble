
@extends('layouts.plantilla-full',
    ['titulo' => 'Scrabble: Dashboard', 'columnas'=>'3', 'reversed'=>'true', 'css'=>'dashboard', 'js'=>['scr_dashboard']])
@section('menus')
@php
    $nombreUser = $user->name;
    $countryUser = $user->country;
    $langUser = $user->favourite_language;
    $avatar = $user->avatar == "" ? "/img/girlvatar.png" : $user->avatar ;
    $datosUser = (object) ['nombre'=> $nombreUser,'pais'=> $countryUser,'idioma'=>$langUser];
@endphp
    <div class="menu--dashboard " role="navigation" aria-label="Menu usuario">
        <ul class="menu--gris">
            
            <!--determinar en created si hay o no y mostrar en funcion de ello-->
            <li class="list-group-item" @click="hazVisible('partidas')"><i class="fas fa-play" aria-hidden="true"></i><span>Partidas</span></li>
            <li class="list-group-item"  @click="hazVisible('stats')"><i class="fas fa-chart-pie" aria-hidden="true"></i><span>Estadísticas</span></li>
            <li class="list-group-item" @click="hazVisible('historico')"><i class="fas fa-history" aria-hidden="true"></i> <span> Histórico</span></a></li>
            <!--si no hay mensajes nuevos open, si los hay, cerrado-->
            <li class="list-group-item" @click="hazVisible('notificaciones')">
                <i :class="{ 'fas fa-envelope' : cuantasNotificaciones > 0, 'fas fa-envelope-open':cuantasNotificaciones <= 0 }" aria-hidden="true"></i>
                <span>Notificaciones</span>
                <badge-component v-show="cuantasNotificaciones > 0" clase="info--notifications" datos="{{count($notifications)}}" :datosnuevos="cuantasNotificaciones"></badge-component>
            </li>
            <li class="list-group-item" @click="hazVisible('perfil')"><i class="fas fa-address-card" aria-hidden="true"></i><span>Editar Perfil</span></li>
            <li class="list-group-item" @click="logout()"><i class="fas fa-sign-out-alt" aria-hidden="true"></i><span>Cerrar Sesion</span></li>

        </ul>
    </div>

@endsection
@section('aside-derecha')
<!--menu dashboard para móviles-->
<div class="menu-dashboard--slim" role="navigation" aria-label="Menu usuario">
    <ul>
        <li title="Perfil" @click="hazVisible('perfil')"> 
            <img src="{{$avatar}}" title="Perfil" class="rounded-avatar">
        </li>
        <!--meter numeros y sobres-->
        <li title="Notificaciones" class=""  @click="hazVisible('notificaciones')">
        <i class="fas fa-envelope-open fa-3x list-icon--slim" aria-hidden="true"></i>
        </li>
        <li title="Estadísticas" class=""  @click="hazVisible('stats')"><i class="fas fa-chart-pie fa-3x list-icon--slim" aria-hidden="true"></i>
        </li>
        <li title="Historial de Partidas" class="" @click="hazVisible('historico')">
        <i class="fas fa-history fa-3x list-icon--slim" aria-hidden="true"></i> 
        </li>
        <li title="Partidas en Curso" class="" @click="hazVisible('partidas')"><i class="fas fa-play fa-3x list-icon--slim" aria-hidden="true"></i></li>
        <li title="Nueva Partida" class="" @click="hazVisible('partidaRapida')"><i class="fas fa-plus fa-3x list-icon--slim" aria-hidden="true"></i></li>
    </ul>
</div>
<!--cambiar los datos de usuario para cogerlos de la variable blade inicialmente
     lo hago asi y no con un metodo porque asi no se esta ejecutando cada vez q se re-renderiza la pagina-->
    <user-info :rutanueva="nuevaRutaAvatar" :rutaavatar={{json_encode($avatar)}} :datosusuario="{{json_encode($datosUser)}}" :datosusuarionuevos="datosNuevosusuario" ></user-info>
    <partida-rapida @new-challenge="updateRetos" class="display--small" v-if="!visibilidad.partidaRapida" claseformulario="caja-formulario" :uid="{{$user->id}}" :usuales="{{$usualopponents}}"></partida-rapida>
@endsection
@section('contenido') 
<!--uso v-if en vez de v-show porque asi cuando se vuelve a crear el componente hace las llamadas y se obtienen los datos actualizados-->
    <info-partidas  v-show="visibilidad.partidas" :datosactualizados="nuevosChallenges" :datospartidas="{{$games}}" :datoschallenges="{{ json_encode($challenges) }}"></info-partidas>
    <user-stats v-if="visibilidad.stats" class="display--big"></user-stats>
    <historico-partidas v-if="visibilidad.historico" paginationclass="letra" :userid="{{$user->id}}"></historico-partidas>
    <notifications-component @cambio-noleidos="updateNotificaciones" v-show="visibilidad.notificaciones" :datos="{{$notifications}}"></notifications-component>
    <edit-user @cambio-datos="updatePerfil" @cambio-avatar="updateAvatar" v-show="visibilidad.perfil" ></edit-user>
    <!-- le pongo v-if para q no haya 2 compoenntes IGUALES a la vez-->
    <partida-rapida  @new-challenge="updateRetos" v-if="visibilidad.partidaRapida" claseformulario="caja-formulario" :uid="{{$user->id}}" :usuales="{{$usualopponents}}"></partida-rapida>
@endsection