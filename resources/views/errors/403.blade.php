@extends('layouts.plantilla-errores',
    ['titulo' => '403 FORBIDDEN', 'error'=>'403', 'css'=>'403'])
@section('contenidoError')
<p>No tienes permiso para acceder a esta página. ¿Porque no vuelves al <a href="/" title="inicio">incio</a> o haces <a href="/scrabble/login" title="login">login</a> con el usuario adecuado?</p>
@endsection

