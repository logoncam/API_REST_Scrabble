

@extends('layouts.app',
    ['title' => 'Dashboard', 'css_files' => ['test_scr_dashboard'], 
    'js_files' => ['test_scr_home']])

@section('content')

@php
    if (is_null($user->avatar)) $avatar = "";
    else $avatar = $user->avatar;
@endphp
    <dashboard-test-component
        :user="{{ json_encode($user) }}"
        :avatar="{{ json_encode($avatar) }}"
        :variables="{{ json_encode($statistics) }}">
    </dashboard-test-component> 

    <!-- form oculto para realizar el logout via POST de manera síncrona. Podría haber utilizado la función post
        que está definida en test_helpers -->
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    <ul>
                <li class="input-menu"><a href="{{ route('register') }}">Registro</a></li>
                <li class="input-menu upper-margin"><a href="{{ route('login') }}">Login</a></li>
                <li class="input-menu upper-margin"><a v-on:click.prevent="ranking" href="#">Ranking usuarios</a></li>
                <li class="input-menu"><a v-on:click.prevent="currentGames" href="#">Últimas partidas en juego</a></li>
                <li class="input-menu"><a v-on:click.prevent="generalInfo" href="#">Información sobre el sistema</a></li>
            </ul>

@endsection
