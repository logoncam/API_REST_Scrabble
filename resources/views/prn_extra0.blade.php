@extends('layouts.plantilla-estatica',
    ['titulo' => 'Gutenberg'])

@section('contenido')
<div class="row">
    <div class="col">
    <img class ="rounded-circle float-left w-25 mr-5" src="{{asset('img/gutemberg.jpg')}}" alt="Grabado del señor Gutenberg"/>
    <p>Gutenberg nació en Maguncia, Alemania alrededor de1400   en   la   casa   paterna   llamada   zum   Gutenberg.   Suapellido   verdadero   es   Gensfleisch   (en   dialecto   alemánrenano este apellido tiene semejanza, si es que no significa,«carne de ganso», por lo que el inventor de la imprenta enOccidente prefirió usar el apellido por el cual es conocido).Hijo del comerciante Federico Gensfleisch, que adoptaríaposteriormente hacia 1410 el apellido zum Gutenberg, y deElse Wyrich, hija de un tendero</p>
    <p>Conocedor del arte de la fundición del oro, se destacócomo herrero para el obispado de su ciudad. La familia setrasladó a Eltville am Rhein, ahora en el Estado de Hesse,donde   Else   había   heredado   una   finca.   Debió   haberestudiado   en   la   Universidad   de   Erfurt,   en   donde   estáregistrado en 1419 el nombre de Johannes de Alta Villa(Eltvilla). Ese año murió su padre. Nada más se conoce deGutenberg hasta que en 1434 residió como platero en Estrasburgo, donde cinco años después se vioenvuelto en un proceso, que demuestra de forma indudable, que Gutenberg había formado unasociedad con Hanz Riffe para desarrollar ciertos procedimientos secretos. En 1438 entraron comoasociados Andrés Heilman y Andreas Dritzehen (sus herederos fueron los reclamantes) y en elexpediente judicial se mencionan los términos de prensa, formas e impresión</p>
    <p>De regreso a Maguncia, formó una nueva sociedad con Johann Fust, quien le da un préstamo conel que, en 1449, publicó el Misal de Constanza, primer libro tipográfico del mundo occidental.Recientes publicaciones, en cambio, aseguran que este misal no pudo imprimirse antes de 1473debido a la confección de su papel, por lo que no debió ser obra de Gutenberg. En 1452, Gutenbergda comienzo a la edición de la Biblia de 42 líneas (también conocida como Biblia de Gutenberg). En1455, Gutenberg carecía de solvencia económica para devolver el préstamo que le había concedidoFust, por lo que se disolvió la unión y Gutenberg se vio en la penuria (incluso tuvo que difundir elsecreto de montar imprentas para poder subsistir).</p>
    <p>Johannes Gutenberg murió arruinado en Maguncia, Alemania el 3 de febrero de 1468. A pesar dela oscuridad de sus últimos años de vida, siempre será reconocido como el inventor de la imprenta moderna.</p>
</div>

</div>
@endsection