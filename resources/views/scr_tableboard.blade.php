@extends('layouts.plantilla-full',
    ['titulo' => 'Scrabble: Partida', 'columnas'=>'3', 'reversed'=>'true', 'css'=>'dashboard', 'js'=>['scr_tablero']])

@section('menus')
<div class="menu--dashboard " role="navigation" aria-label="Menu usuario">
    <ul class="menu--gris">
        <li class="list-group-item"><i class="fas fa-user" aria-hidden="true"></i><a href="{{ route('dashboard') }}">DashBoard</a></li>
        <li class="list-group-item" @click.once="logout()"><i class="fas fa-sign-out-alt" aria-hidden="true"></i><span>Cerrar Sesion</span></li>
    </ul>
</div>
@endsection

@section('aside-derecha')
<info-partida :usuario="{{json_encode($user)}}" :oponente="{{json_encode($opponent)}}" :game="{{json_encode($game)}}" :datos-actualizados="datosActualizados"/>
@endsection
@section('contenido') 
<area-juego :user="{{json_encode($user)}}" :game="{{json_encode($game)}}" @cambio-estado="updateData" />
@endsection