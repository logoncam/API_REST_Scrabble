# PLANIFICACIÓN
# Personas
## #1

**Nombre**: Julia Martínez.  
**Edad**: 17.  
**Ocupación**: Estudiante de 2º de Bachiller en el CEEDCV.  
**Descripción**: Está acostumbrada al uso del móvil y su portátil para acceder a la información que le brinda internet. Odia los juegos de mesa, y en sus ratos libres sale a correr.  
**Escenario**: Le han mandado un trabajo sobre la imprenta y los cambios que este invento trajo consigo, su profesor les ha dicho que el centro ha inaugurado una web conmemorativa y accede para poder informarse y ver si puede encontrar más sitios bibliográficos de interés. Aprovecha el viaje en autobús a casa durante esta tarde para leer desde el móvil la información.  

## #2

**Nombre**: Joao Pereira  
**Edad**: 45  
**Ocupación**: Desempleado, estudiante de Español.  
**Descripción**:  Joao quiere mudarse a España y por eso está aprendiendo Español, tiene buen nivel y está casi listo. Su manejo de los dispositivos electrónicos no es muy allá, pero domina los juegos del móvil que le instaló su mujer  y busca cosas en google con la tablet.  
**Escenario**: En una semana Joao tiene su examen del C1 en el instituto Cervantes de su ciudad, aunque confía en sus capacidades su profesor le acaba de aconsejar que amplíe su vocabulario jugando contra españoles nativos al Scrabble. Mientras cena, busca en internet con su tablet y encuentra una página sobre el Scrabble con estudiantes españoles de un centro llamado Ceedcv ¡Justo lo que necesitaba!  

## #3

**Nombre**: Rafael de la Calle.  
**Edad**: 74  
**Ocupación**: Jubilado.  
**Descripción**: Hace poco que su hija le ha regalado un teléfono móvil y está aprendiendo a utilizarlo. Es una persona muy curiosa y cree que le podrá sacar utilidad al regalo aunque en un principio renegaba de utilizar uno.  
**Escenario**: Una tarde al calor del brasero, desde la comunidad de su sillón orejero, estaba Rafael renegando porque no podía salir a podar los frutales debido a la lluvia.  
Buscaba información desde su móvil sobre los primeros libros impresos en España para pensar en otra cosa y, tras bosquejar los primeros resultados de búsqueda ha ido a dar con una página sobre la imprenta que le da la misma información que las demás, por lo que decide abandonarla y seguir con otra cosa.  

## #4

**Nombre**: María Rosa Amor.  
**Edad**: 34.  
**Ocupación**: Profesora del departamento de Lengua y literatura en el Ceedcv.  
**Descripción**: Madre de dos hijos, no tiene tiempo de más para preparar las cosas de sus alumnos, así que utiliza todo lo que los recursos del Ceedcv le pueden ofrecer para amenizar la didáctica.  
**Escenario**: En el momento que su compañera de departamento le contó sobre la web de Gutenberg y el Scrabble que había en ella, María Rosa tuvo claro que es lo que iba a hacer para motivar a sus alumnos en la adquisición de nuevo vocabulario y mejora de la ortografía: ¡Aquellos que a fin de curso tuvieran más puntuación que ella en el Ranking tendrían un punto extra! .  
Pero quiere empezar la casa por el tejado, así que ahora que los niños duermen, accederá, se registrará y jugará unas cuantas partidas desde el ordenador de casa antes de anunciarlo a sus estudiantes.  

# Mapa Conceptual
## Competición de Scrabble

![mapa conceptual](imagenes/conceptualMapa.png "mapa conceptual")  

# Mapa del sitio web

![mapa sitio web](imagenes/mapasitio.png "sitemap")  

# Diagramas de Flujo
### Desarrollo partida Scrabble

![diagramas de flujo](imagenes/flujoDiagram.png "diagramas de flujo")  

# Bocetos Alámbricos
## Texto: Página de inicio, Gutemberg, imprenta en España

![texto boceto](imagenes/gut_text.jpg "textos")

## Tablero Scrabble

![tablero juego](imagenes/tablero.jpg "tablero")

## Dashboard Scrabble

![dashboard horizontal](imagenes/Dashboard_horizontal.png "dashboard scrabble")
![dashboard vertical](imagenes/verticaldashboard.jpg "dashboard vertical")

### Detalle Capa Dashboard Vertical

![dashboard capa](imagenes/detalledashboardcapavert.jpg "dashboard capa")

 
## Scrabble: Información general

![info Scrabble](imagenes/infoscrabble.jpg "info scrabble")

# Registro/Login

![registro](imagenes/registro.jpg "registro jugadores") 

