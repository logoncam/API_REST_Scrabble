<!DOCTYPE html>
<html lang="en">
<x-header titulo="{{$titulo}}" css="{{$css ?? ' '}}"/>
<body>
<div class="container">
    <x-navegacion/>
    <x-error error="{{$error}}" titulo="{{$titulo}}">
    @if (isset($cols) && isset($rows))
        <x-slot name="puzzle">
            @yield('puzzle')
        </x-slot>
    @endif
        <x-slot name="contentError">
            @yield('contenidoError')
        </x-slot>
    </x-error>
    <x-pie/>
</div>
</body>
</html>