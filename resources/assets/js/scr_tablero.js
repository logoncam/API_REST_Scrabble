//copypaste de la muerte
import Echo from "laravel-echo";
import axios from "axios";
//Lo comento porque sino empieza a lanzar warnings de la muerte
//porque parece q esta duplicado somewhere...
//import Vue from "vue";

window.io = require("socket.io-client");

Vue.component("InfoPartida", require("./components/InfoPartida.vue").default);
Vue.component("AreaJuego", require("./components/AreaJuego.vue").default);

new Vue({
  el: ".container-fluid",
  data: {
    datosActualizados: undefined,
  },
  //reintentar con sync
  methods: {
    updateData(d) {
      this.datosActualizados = d;
    },
    logout() {
      return axios.post("/scrabble/logout").then(() => {
        window.location.href = "/scrabble/";
      });
    },
  },
});
