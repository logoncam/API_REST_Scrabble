<!--<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Error 404</title>
</head>
<body>
    <p>Página no encontrada</p>
</body>
</html>-->

@extends('layouts.plantilla-errores',
    ['titulo' => '404 NO ENCONTRADO', 'error'=>'404', 'css'=>'404'])
@section('contenidoError')
<p>Página no encontrada, inténtalo de nuevo</p>
@endsection

