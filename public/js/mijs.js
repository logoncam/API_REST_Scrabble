$().ready(() => {
  let idsMapas = ["#1469", "#1470", "#1471", "#1472", "#1474", "#1476"];
  cambiaMapa(idsMapas);
  cambiaLista();
  getNoticiasCeedcv();
  $("#loginForm").submit(validaForm);
});
function cambiaMapa(idEntrada) {
  for (let i = 0; i < idEntrada.length; i++) {
    $(idEntrada[i]).on("click", cambiaImagen);
  }
}
function cambiaImagen() {
  //array con las clases que tienen los distintos mapas
  let ABorrarYears = [
    "img-1469",
    "img-1470",
    "img-1471",
    "img-1472",
    "img-1476",
    "img-1474",
  ];
  //la clase que se le va añadir ( que se construye añadiendo img al id del event target/this)
  let claseDestino = "img-" + this.id;

  borrarClases($(":not(#" + this.id + ")"), ["active"]);
  $(this).addClass("active");

  borrarClases($(".img-mapa"), ABorrarYears);
  $(".img-mapa").addClass(claseDestino);
}
function borrarClases(elemento, clases) {
  for (let i = 0; i < clases.length; i++) {
    if (elemento.hasClass(clases[i])) {
      elemento.removeClass(clases[i]);
    }
  }
}
function cambiaLista() {
  $(".img-europa").on("animationstart", activaCiudades);
}
function activaCiudades() {
  //eso es para q dure algo con esa clase activada...
  $("#1469").addClass("active");
  //como la animacion dura 1000ms si hace el intervalo en sus multiplos no se sincronizaria bien.
  //o algo asi
  let intervalo = setInterval(() => activaLista(intervalo), 229);
  $(".img-europa").on("animationend", () => finAnimacionMapa(intervalo));
  $(".img-europa").on("mouseleave", () => finAnimacionMapa(intervalo));
}
function activaLista() {
  let content = $(".img-europa").css("content").replaceAll('"', "");
  let actContent = "";
  if (content !== "normal" && actContent !== content) {
    actContent = content;
    $("#mapaeuropa li").removeClass("active");
    $(content).addClass("active");
  }
}

function finAnimacionMapa(intervalo) {
  clearInterval(intervalo);
  $("#mapaeuropa li").removeClass("active");
}
/**NOTICIAS CEEDCV */

function getNoticiasCeedcv() {
  const url = "http://localhost/api/info/news/0";
  $.getJSON(url)
    .done((msg) => {
      construirNoticias($(".noticia"), msg, construyeCompleta);
      construirNoticias($(".link-news.bar"), msg, construyeSoloTitular);
    })
    .fail(() => {
      $(".noticia").css("display", "none");
      $(".noticia").parent.html("<p> No se han podido cargar las noticias</p>");
      $(".barra--news").css("display", "none");
    });
}
function construirNoticias(bancoNoticias, datos, funcion) {
  let contador = 0;
  for (let noticia of bancoNoticias) {
    funcion(noticia, datos[contador]);
    contador++;
  }
}
function construyeCompleta(donde, datos) {
  $(donde).find(".link-news").html(datos.header);
  $(donde).find(".link-news").attr("href", datos.url);
  if (datos.abstract.length > 160) {
    datos.abstract = datos.abstract.substring(0, 200) + "...";
  }
  $(donde).find(".resumen").html(datos.abstract);
  $(donde).find(".date").html(datos.updated_at);
}
function construyeSoloTitular(donde, datos) {
  $(donde).html(datos.header);
  $(donde).attr("href", datos.url);
}

/***LOGIN MODAL */
function autenticacion() {
  $.ajax({
    url: "/scrabble/login",
    method: "POST",
    data: { email: $("#email").val(), password: $("#pw").val() },
  })
    .done((msg) => {
      $(location).attr("href", "/scrabble/dashboard");
      //eso no me gusta proque no me lleva al dashboard, solo reemplaza
      // document.write(msg);
    })
    .fail((err) => {
      let error = JSON.parse(err.responseText);
      $("#mensajeLogin").html(error.errors.email);
      $("#pw , #email").addClass("error");
    });
}

function valida(exp, id) {
  let element = $(id);
  const regExpresion = new RegExp(exp);
  const testValor = regExpresion.test(element.val());
  if (!testValor) {
    $("#mensajeLogin").html("Completa los datos");
    element.addClass("error");
    return false;
  } else {
    element.removeClass("error");
    return true;
  }
}

function validaForm(ev) {
  ev.preventDefault();
  let mailValido = valida(
    "([a-zA-Z]|[0-9])+@(([a-zA-Z]|[0-9])+).[a-zA-Z]*",
    "#email"
  );
  let pwValida = valida("\\S{5,}", "#pw");
  if (mailValido && pwValida) {
    autenticacion();
  }
}
