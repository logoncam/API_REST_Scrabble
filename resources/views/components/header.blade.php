@props(['titulo','css'])

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=M+PLUS+1p&family=Special+Elite&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/misestilos.css?')}}">
    <!-- mover solo a la plantilla q tenga el timeline-->
    <link rel="stylesheet" href="{{asset('css/timeline.css?')}}">
    <!-- esto ahora no m sirve porque no puedo meter arrays, tengo q sacar un slot-->
    <script src="https://kit.fontawesome.com/5c34057b48.js" crossorigin="anonymous"></script>
    <title>{{$titulo}}</title>
    @if (isset($css) )
         <link rel="stylesheet" href="{{asset('css/'.$css.'.css?')}}">  
    @endif
    @csrf

</head>