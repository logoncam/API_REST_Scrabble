import axios from "axios";
import Vue from "vue";
import PAISES from "./paises";
Vue.component(
  "TablaComponent",
  require("./components/TablaComponent.vue").default
);
new Vue({
  el: ".container-fluid",
  data: {
    partidascurso: [],
    mostvalueword: {},
    toptenspanish: [],
    toptenenglish: [],
    englishReady: false,
    spanishReady: false,
  },
  computed: {
    top10: function () {
      //hasta q no están listas las dos llamadas no puede mezclar bien los arrays
      if (this.englishReady && this.spanishReady) {
        let concatarr = this.toptenenglish
          .concat(this.toptenspanish)
          .sort((a, b) => a.user_id - b.user_id);
        return this.sumids(concatarr);
      } else {
        return [];
      }
    },
  },
  created: function () {
    this.getLastMovs();
    this.getMostValWord();
    this.getTopTen("es");
    this.getTopTen("en");
  },
  methods: {
    llamadaServidor(url, callback) {
      return axios
        .get(url)
        .then(function (response) {
          callback(response);
        })
        .catch(function (error) {
          alert(error);
          console.log(error);
        });
    },
    //functiones para obtener los ultimos movimientos
    getLastMovs() {
      this.llamadaServidor("/api/info/currentgames/3", (response) => {
        response.data.forEach((element) => {
          let obj = {
            Player1: {
              src: this.setAvatar(element.player1.avatar),
              texto: element.player1.name,
            },
            Player2: {
              src: this.setAvatar(element.player2.avatar),
              texto: element.player2.name,
            },
            Puntuacion: element.player_1_score + " - " + element.player_2_score,
          };
          this.partidascurso.push(obj);
        });
      });
    },
    setAvatar(url) {
      return url === null ? "/img/girlvatar.png" : url;
    },
    //funciones para obtener la palabra más valiosa
    getMostValWord() {
      this.llamadaServidor("/api/info/general", (response) => {
        let palabraMayor = response.data.statistics[0].sort(
          (a, b) => b.most_valuable_word_points > a.most_valuable_word_points
        )[0];
        //revisar para saber todos los reemplazos q hay q hacer
        this.mostvalueword.word = palabraMayor.most_valuable_word.replace(
          "n",
          "Ñ"
        );
        this.mostvalueword.points = palabraMayor.most_valuable_word_points;
      });
    },
    //functiones para obtener el top ten
    getTopTen(lang) {
      //hago dos llamadas para obtener el top 10 de los dos idiomas
      this.llamadaServidor("/api/info/ranking/" + lang + "/10", (response) => {
        if (lang === "es") {
          this.toptenspanish = response.data;
          this.spanishReady = true;
        } else {
          this.toptenenglish = response.data;
          this.englishReady = true;
        }
      });
      //una vez asignadas las llamadas el computed se encarga del resto
    },
    //busca los ids repetido y suma las partidas ganadas, devuelve un array de 10 objetos con usuarios unicos
    //ordenados de mayor a menor puntuacion
    sumids(arrayConcat) {
      let newArr = [];
      for (let i = 0; i < arrayConcat.length - 1; i++) {
        let newObj = this.creaObjeto(arrayConcat[i]);
        newArr.push(newObj);
        if (arrayConcat[i].user_id === arrayConcat[i + 1].user_id) {
          let newPuntos = arrayConcat[i].won + arrayConcat[i + 1].won;
          newObj.won = newPuntos;
          i++;
        }
      }
      newArr = newArr.sort((a, b) => b.won - a.won).slice(0, 10);
      newArr.forEach((e, index) => (e.posicion = index + 1));
      return newArr;
    },
    //crea el objeto que se usara para el top ten
    creaObjeto(origen) {
      return {
        posicion: 0,
        jugador: {
          src: this.setAvatar(origen.user.avatar),
          texto: origen.user.name,
        },
        won: origen.won,
        //nombre completo!
        country: this.setCountry(origen.user.country),
      };
    },
    /**esto se repite mogollon y deberian ser funciones globales o algo asi */
    setCountry(code) {
      if (Object.prototype.hasOwnProperty.call(PAISES, code.toUpperCase())) {
        return PAISES[code.toUpperCase()];
      } else {
        return code;
      }
    },
  },
});
