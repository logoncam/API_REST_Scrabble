
## IMÁGENES
### Orígenes y créditos

* Fotografía de Placa imprenta Palmart. Foto de José M. Marín. Con autorización de publicación
* Fotografía de la Calle San Vicente numero 3 de València. Foto de José M. Marín. Con autorización de publicación
* Fotografía de la Placa imprenta Patricio Mey. Foto de José M. Marín. Con autorización de publicación
* Fotografía del Portal Valldigna. València. Foto de José M. Marín. Con autorización de publicación.

* https://commons.wikimedia.org/wiki/Category:Johannes_Gutenberg
* https://es.wikipedia.org/wiki/Sutra_del_diamante#/media/File:Jingangjing.png
* https://en.wikipedia.org/wiki/History_of_printing#/media/File:Hongfo_Pagoda_woodblock_B.jpg
* http://www.racv.es/files/Discurso-Ricardo-J-Vicent-Museros.pdf
* https://pixabay.com/es/fuente-juego-de-plomo-letras-1889146/
* https://pixabay.com/es/lavado-gancho-de-ángulo-705674/
* https://upload.wikimedia.org/wikipedia/commons/3/32/Prensa_de_Gutenberg._Réplica..png
* https://upload.wikimedia.org/wikipedia/commons/a/af/Buchdruck-15-jahrhundert_1.jpg
* https://de.wikipedia.org/wiki/Datei:Press1520.png
* https://upload.wikimedia.org/wikipedia/commons/4/42/Chodowiecki_Basedow_Tafel_21_c_Z.jpg
* https://pixabay.com/es/europa-pa%C3%ADses-mapa-3d-rojo-rosa-151589/
* http://bvpb.mcu.es/es/consulta/registro.cmd?id=405381
* http://bdh-rd.bne.es/viewer.vm?id=0000177130&page=1
* https://commons.wikimedia.org/wiki/Category:Valencian_Bible#/media/File:Valencian_Bible.JPG
* https://es.wikipedia.org/wiki/Archivo:Valencia.Mercado_Central.jpg
* https://es.wikipedia.org/wiki/Monasterio_de_Santa_Mar%C3%ADa_del_Puig#/media/File:Elpuig_monestir.jpg
* Foto de Fondo creado por freepik (Scrabble FUN) [www.freepik.es](https://www.freepik.es/fotos/fondo)

## FUENTES

* Special Élite (google Fonts)
* M PLUS 1p (google Fonts)

## ICONOS

* Icons made by [Freepik](https://www.flaticon.com/authors/freepik) from [www.flaticon.com](https://www.flaticon.com/)
* Resto de iconos de [Font Awesome](https://fontawesome.com/)

## TEXTOS

* Definiciones de Scrabble extraidas de [Wikipedia](https://es.wikipedia.org/wiki/Scrabble)

## SNIPPETS

* News Feed adaptado de : [BBBBoostrap Team](https://bbbootstrap.com/snippets/bootstrap-news-feed-share-icon-67618451)
* Video Layer adaptado de : [StartBoostrap](https://startbootstrap.com/snippets/video-header)
* Time Line Scrabble adaptado de [Nils Wittler](https://codepen.io/NilsWe/pen/FemfK)
* Border gradient de [CSS Tricks](https://css-tricks.com/gradient-borders-in-css/)