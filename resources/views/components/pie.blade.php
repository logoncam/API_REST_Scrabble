<footer role="contentinfo" aria-label="Creditos">   
    <div class=" row justify-content-start mt-2">
        <hr class="my-4">
        <div class="col text-left text-lg-right">
        <p> Diseño de Loli González Campos  realizado para la asignatura de Diseño de interfaces web 20-21 con sangre, sudor y lágrimas. </p>
        <p>Licencias y créditos en <a  data-toggle="modal" data-target="#creditos" href="#">este enlace</a></p>
        </div>
    </div>
</footer>
<div class="modal" tabindex="-1" id="creditos">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Créditos y Licencias</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ">

        <h4 class="titulos">Imágenes: origenes y créditos</h4>
        
        <ul class="fa-ul">
            <li> <span class="fa-li"><i class="fas fa-link"></i></span> <a href="https://commons.wikimedia.org/wiki/Category:Johannes_Gutenberg" target="_blank" title="Foto Bio Gutenberg">Foto Bio Gutenberg</a></li>
            <li> <span class="fa-li"><i class="fas fa-link"></i></span> <a href="https://es.wikipedia.org/wiki/Sutra_del_diamante#/media/File:Jingangjing.png" target="_blank" title="Foto Bio Gutenberg">Foto Bio Gutenberg</a></li>
            <li> <span class="fa-li"><i class="fas fa-link"></i></span> <a href="https://en.wikipedia.org/wiki/History_of_printing#/media/File:Hongfo_Pagoda_woodblock_B.jpg" target="_blank" title="Pagoda Woodblock"> Pagoda Woodblock</a></li>
            <li> <span class="fa-li"><i class="fas fa-link"></i></span> <a href="http://www.racv.es/files/Discurso-Ricardo-J-Vicent-Museros.pdf" target="_blank" title="Fundidor">Fundidor</a></li>
            <li> <span class="fa-li"><i class="fas fa-link"></i></span> <a href="https://pixabay.com/es/fuente-juego-de-plomo-letras-1889146/" target="_blank" title="Letras Plomo">Letras Plomo</a></li>
            <li> <span class="fa-li"><i class="fas fa-link"></i></span> <a href="https://pixabay.com/es/lavado-gancho-de-ángulo-705674/" target="_blank" title="Gancho de ángulo">Gancho de ángulo</a> </li>
            <li> <span class="fa-li"><i class="fas fa-link"></i></span> <a href="https://upload.wikimedia.org/wikipedia/commons/3/32/Prensa_de_Gutenberg._Réplica..png" target="_blank" title="prensa de gutenberg">prensa de gutenberg</a></li>
            <li> <span class="fa-li"><i class="fas fa-link"></i></span> <a href="https://upload.wikimedia.org/wikipedia/commons/a/af/Buchdruck-15-jahrhundert_1.jpg" target="_blank" title="trabajador 1">trabajador 1</a></li>
            <li> <span class="fa-li"><i class="fas fa-link"></i></span> <a href="https://de.wikipedia.org/wiki/Datei:Press1520.png" target="_blank" title="trabajador 2">trabajador 2</a></li>
            <li> <span class="fa-li"><i class="fas fa-link"></i></span> <a href="https://upload.wikimedia.org/wikipedia/commons/4/42/Chodowiecki_Basedow_Tafel_21_c_Z.jpg" target="_blank" title="trabajador 3">trabajador 3</a></li>
            <li> <span class="fa-li"><i class="fas fa-link"></i></span> <a href="https://pixabay.com/es/europa-pa%C3%ADses-mapa-3d-rojo-rosa-151589/" target="_blank" title="El mapa de europa más feo del universo">mapa de europa</a></li>
            <li> <span class="fa-li"><i class="fas fa-link"></i></span> <a href="http://bvpb.mcu.es/es/consulta/registro.cmd?id=405381" target="_blank" title="libros impresos españa">libros impresos españa</a></li>
            <li> <span class="fa-li"><i class="fas fa-link"></i></span> <a href="http://bdh-rd.bne.es/viewer.vm?id=0000177130&page=1" target="_blank" title="libro impreso">libro impreso</a></li>
            <li> <span class="fa-li"><i class="fas fa-link"></i></span> <a href="https://commons.wikimedia.org/wiki/Category:Valencian_Bible#/media/File:Valencian_Bible.JPG" target="_blank" title="Biblia Valenciana">Biblia Valencian</a></li>
            <li> <span class="fa-li"><i class="fas fa-link"></i></span> <a href="https://es.wikipedia.org/wiki/Archivo:Valencia.Mercado_Central.jpg" target="_blank" title="Mercado Central de Valencia">Mercado Central de Valencia</a></li>
            <li> <span class="fa-li"><i class="fas fa-link"></i></span> <a href="https://es.wikipedia.org/wiki/Monasterio_de_Santa_Mar%C3%ADa_del_Puig#/media/File:Elpuig_monestir.jpg" target="_blank" title="Monasterio del Puig">Monasterio del Puig</a></li>
            <li> <span class="fa-li"><i class="fas fa-link"></i></span> <a href="https://www.freepik.es/fotos/fondo">Foto de Fondo creado por freepik (Scrabble FUN) - www.freepik.es</a></li>
        </ul>
        <ul class="fa-ul">
            <li><span class="fa-li"><i class="far fa-image"></i></span> Fotografía de Placa imprenta Palmart. Foto de José M. Marín. Con autorización de publicación</li>
            <li><span class="fa-li"><i class="far fa-image"></i></span> Fotografía de la Calle San Vicente numero 3 de València. Foto de José M. Marín. Con autorización de publicación</li>
            <li><span class="fa-li"><i class="far fa-image"></i></span> Fotografía de la Placa imprenta Patricio Mey. Foto de José M. Marín. Con autorización de publicación</li>
            <li><span class="fa-li"><i class="far fa-image"></i></span> Fotografía del Portal Valldigna. València. Foto de José M. Marín. Con autorización de publicación.</li>
        </ul>
        <h4 class="titulos">Fuentes</h4>
        <ul class="fa-ul">
                <li><span class="fa-li"><i class="fab fa-fonticons"></i></span>Special Élite (google Fonts)</li>
                <li> <span class="fa-li"><i class="fab fa-fonticons"></i></span>M PLUS 1p (google Fonts)</li>
            </ul>
        <h4 class="titulos">Iconos</h4>
            <ul class="fa-ul">
                <li><span class="fa-li"><i class="fas fa-link"></i></span>Icons made by <a href="https://www.flaticon.com/authors/freepik"  target="_blank" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></li>
                <li> <span class="fa-li"><i class="fas fa-link"></i></span>Resto de iconos de <a href="https://fontawesome.com/" target="_blank" title="fontawesome"> Font Awesome</a></li>
            </ul>
        <h4 class="titulos">Textos</h4>
        <ul class="fa-ul">
                <li><span class="fa-li"><i class="fas fa-link"></i></span> Definiciones de Scrabble extraidas de  <a href="https://es.wikipedia.org/wiki/Scrabble" title="Scrabble">Wikipedia</a> </li>
            </ul>
        <h4 class="titulos">Snippets</h4>
            <ul class="fa-ul">
                <li><span class="fa-li"><i class="fas fa-code"></i></span> News Feed adaptado de   <a href="https://bbbootstrap.com/snippets/"  target="_blank" title="BBBBoostrap Team">BBBBoostrap Team</a> </li>
                <li><span class="fa-li"><i class="fas fa-code"></i></span> Video Layer adaptado de  <a href="https://startbootstrap.com/snippets/video-header" target="_blank" title="StartBoostrap">StartBoostrap</a> </li>
                <li><span class="fa-li"><i class="fas fa-code"></i></span> TimeLine Scrabble adaptado de  <a href="https://codepen.io/NilsWe/pen/FemfK"  target="_blank" title="Nils Wittler">Nils Wittler</a> </li>
                <li><span class="fa-li"><i class="fas fa-code"></i></span> Border Gradient de  <a href="https://css-tricks.com/gradient-borders-in-css/"  target="_blank" title="CSS Tricks">CSS Tricks</a> </li>

            </ul>

      </div>
    </div>
  </div>
</div>
    <a id="back-to-top" href="#top" class="btn btn-dark btn-sm back-to-top" role="button"><i class="fas fa-chevron-up"></i></a>

<script  type="application/javascript"  src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script  type="application/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script  type="application/javascript"  src="{{asset ('js/mijs.js')}}"></script>
