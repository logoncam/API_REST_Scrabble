
    @extends('layouts.plantilla-full',
    ['titulo' => 'Scrabble: El juego', 'columnas'=>'3','js'=>['src_scrabbleindex']])
   
    @section('aside-derecha')
    <div class="row justify-content-center" aria-live="polite" >
       <!-- <h4 class="titulos pb-4">Ya somos
            <span class="contador"><span>1</span></span>
            <span class="contador"><span id="dos">2</span></span>
            <span class="contador"><span id="tres">8</span></span>
        </h4>-->
        <h4 class="titulos pb-4">Ya somos
        @php
        $stringUsers = strval($numberUsers);
        @endphp
        @for ($i =0 ; $i < strlen($stringUsers) ; $i++ )
        <span class="contador">
            <span>
                {{ $stringUsers[$i] }}
            </span>
        </span>
        @endfor
        </h4>
      
        <p class="d-none d-lg-block">Únete a la comunidad global de Scrabble en Castellano y mejora tus habilidades compitiendo con <span> los que sean </span> usuarios que disfrutan 
       cada día de este clásico juego de sobremesa.</p>
       @auth
       <button class="btn btn-lg w-75 button-dark-orange"><a href="/scrabble/dashboard/"> Jugar Ahora </a></button>
       @endauth
       @guest
       <button class="btn btn-lg w-75 button-dark-orange"><a href="{{route('register') }}"> ¡Apúntate! </a></button>

       @endguest
    </div>
    
        <ul class="list-group menu--gris mt-5" role="navigation" aria-label="contenido">
            <li class="list-group-item "> <i class="fab fa-researchgate"></i><a data-toggle="collapse" href="#scrabbleDesc"> ¿Qué es Scrabble?</a></li>
            <li class="list-group-item "> <i class="fas fa-tasks"></i><a data-toggle="collapse" href="#reglas"> Cómo Jugar</a></li>
            <li class="list-group-item "> <i class="fas fa-medal"></i><a data-toggle="collapse" href="#ranking"> Ránking</a> </li>
            @guest
            <li class="list-group-item "> <i class="fas fa-user"></i><a href="#"  data-toggle="modal" data-target="#areaUsuarios"> Acceso</a> </li>
            @endguest
            @auth
            <li class="list-group-item "> <i class="fas fa-user"></i><a href="/scrabble/dashboard "> Dashboard</a> </li>
            @endauth
            <li class="list-group-item "> <i class="fas fa-chart-bar"></i><a href="#"  data-toggle="collapse" data-target="#stats"> Ahora Jugando</a> </li>
        </ul>
   
    @endsection
    @section('contenido')
        
        <div id="contenidoPrincipal">
        <div id="stats" data-parent="#contenidoPrincipal" class="collapse show">
            <h2 class="titulos">Comunidad Scrabble del CeedCV</h2>
            <img class="img-fluid img-grises mb-4" src="{{asset('img/fun-scrabble-cropped.jpg')}}" alt="diversion con banderas, digo con letras">
            <p class="d-block d-lg-none">Únete a la comunidad global de Scrabble en Castellano y mejora tus habilidades compitiendo con <span> los que sean </span> usuarios que disfrutan 
            cada día de este clásico juego de sobremesa.</p>
            <div class="row" aria-live="polite">
                <div class="col-12 col-lg-4  mt-4 mt-lg-0">
                     <h5 class="titulos">Info</h5>
                     <p><span class="letra">{{$connectedUsers}}<sub><i class="far fa-smile fa-xs"></i></sub></span> Usuarios en línea .</p>
                     <p><span class="letra">{{$playingGames}}<sub><i class="far fa-smile fa-xs"></i></sub></span> Partidas en juego .</p>
                     <p><span class="letra">{{$languages->count()}}<sub><i class="far fa-smile fa-xs"></i></sub></span> Idiomas disponibles .</p>
                    
                    </div>
                <div class="col-12 col-lg-8 mt-4 mt-lg-0" aria-live="polite">
                <h5 class="titulos">Últimos movimientos</h5>
                <!--sustituir por table component-->
                <tabla-component :datos="partidascurso" :imagenesid="['Player1','Player2']"></tabla-component>
                </div>
               
            </div>
            <div class="row" aria-live="polite">
               <div class="col"> <p>Tenemos 80 jugadores de nivel 1, 30 de nivel 2, 5 de nivel 3, 10 de nivel 4 y 3 de nivel 5. Elige con quién jugar y supéralos.</p></div>
            </div>

        </div>
        <div class="table-responsive collapse" id="ranking" data-parent="#contenidoPrincipal" aria-live="polite">
            <h2 class="titulos">Los 10 mejores </h2>
            <tabla-component v-if="englishReady && spanishReady" 
                :datos="top10" 
                :imagenesid="['jugador']" 
                :headers="['#','Jugador','Partidas Ganadas','Origen']"> 
            </tabla-component>
            <p v-else>Cargando...</p>
    </div>
    <div class="collapse" id="scrabbleDesc" data-parent="#contenidoPrincipal">
        <h2 class="titulos">¿Qué es el Scrabble?</h2>
        <p>Scrabble es un juego de mesa en el cual cada jugador intenta ganar más puntos mediante la <strong>construcción de palabras sobre un tablero</strong> de 15x15 casillas cuadradas. Las palabras pueden formarse, siempre y cuando aparezcan en el diccionario estándar, de forma horizontal o verticalmente y se pueden cruzar. </p>
        <p>En total hay <strong>100 fichas</strong>, 98 marcadas con letras y dos en blanco (sin puntos, actúan como comodines usándose para reemplazar letras). Según su frecuencia de aparición, las letras tienen más o menos puntos, siempre las de mayor frecuencia valen menos.  </p>
        <p>El tablero tiene también casillas de premiación, que multiplican el número de puntos concedidos: las casillas rojo oscuro son de "triple palabra", las rosas "doble palabra", azul oscuro "triple letra" y celeste "doble letra". El casillero central se marca con una estrella y cuenta como casilla de doble palabra; es obligatorio que el juego comience utilizando esta casilla.</p>
        <div class="jumbotron text-center" >
            <h5><i class=" align-middle fas fa-info-circle fa-2x"></i> ¿Sabías qué?</h5>
            <hr class="my-4">
            <span class="letra" v-for="(letra,index ) in mostvalueword.word" :key="index">@{{letra}}</span>
            <!--<div class="letra"> <span class="letra-concreta">M</span><sub>3</sub></div>
            <div class="letra"> <span class="letra-concreta">U</span><sub>8</sub></div>
            <div class="letra"> <span class="letra-concreta">R</span><sub>1</sub></div>
            <div class="letra"> <span class="letra-concreta">C</span><sub>2</sub></div> 
            <div class="letra"> <span class="letra-concreta">I</span><sub>3</sub></div>
            <div class="letra"> <span class="letra-concreta">E</span><sub>8</sub></div>
            <div class="letra"> <span class="letra-concreta">L</span><sub>1</sub></div>
            <div class="letra"> <span class="letra-concreta">A</span><sub>2</sub></div>
            <div class="letra"> <span class="letra-concreta">G</span><sub>2</sub></div> 
            <div class="letra"> <span class="letra-concreta">0</span><sub>2</sub></div> 
            <hr class="my-4">-->
            <p>Es la palabra más valorada que nuestros usuarios han jugado con @{{mostvalueword.points}} puntos <strong>¿Te animas a superarlo? </strong></p>
        </div>
         
    </div>
    <div class="collapse" id="reglas" data-parent="#contenidoPrincipal">
        <h2 class="titulos"> Cómo Jugar al Scrabble</h2>
        <p> El Scrabble es un juego muy sencillo de jugar, ¡y con nuestra aplicación <strong>está todo preparado</strong> para que no tengas que molestarte ni en preparar el tablero!</p>
        <ul class="timeline">
	<!-- Item 1 -->
	<li>
		<div class="direction-r justify-content-right">
			<div class="flag-wrapper">
				<span class="flag titulos">¡Hazte Miembro!</span>
			</div>
			<div class="desc">Únete a la comunidad para disfrutar de nuestro increible juego.</div>
		</div>
	</li>
  
	<!-- Item 2 -->
	<li>
		<div class="direction-l">
			<div class="flag-wrapper">
				<span class="flag titulos">Elige Contrincante</span>
			</div>
			<div class="desc"> Puedes elegir a contrincantes por nivel o por idioma para darle emoción a la partida</div>
		</div>
	</li>

	<!-- Item 3 -->
	<li>
		<div class="direction-r">
			<div class="flag-wrapper">
				<span class="flag titulos">¡Juega!</span>
			</div>
			<div class="desc">En cada turno tendrás disponibles, hasta que se acaben, 7 letras con las que formar palabras en el tablero.</div>
		</div>
    </li>
    <!--item 4-->
    <li>
		<div class="direction-l">
			<div class="flag-wrapper">
				<span class="flag titulos">Regodéate</span>
            </div>
            <div class="desc">Tu nivel, tus victorias, tu palabra más impresionante,... estarán en el Dashboard para que veas siempre tu progreso </div>
		</div>
	</li>
  
</ul>

    </div>
</div>

        
    @endsection    
