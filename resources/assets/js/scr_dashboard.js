import axios from "axios";
import Vue from "vue";
import PAISES from "./paises";
//importo los componentes
Vue.component("InfoPartidas", require("./components/infoPartidas.vue").default);
Vue.component("UserInfo", require("./components/userInfo.vue").default);
Vue.component(
  "NotificationsComponent",
  require("./components/notifications.vue").default
);
Vue.component("EditUser", require("./components/editUserData.vue").default);
Vue.component(
  "HistoricoPartidas",
  require("./components/HistoricoPartidas.vue").default
);
Vue.component("UserStats", require("./components/userStats.vue").default);
Vue.component(
  "PartidaRapida",
  require("./components/partidaRapida.vue").default
);
Vue.component(
  "BadgeComponent",
  require("./components/badgeComponent.vue").default
);

new Vue({
  el: ".container-fluid",
  data: function () {
    return {
      datosNuevosusuario: {},
      nuevaRutaAvatar: "",
      nuevosChallenges: {},
      cuantasNotificaciones: null,
      visibilidad: {
        notificaciones: false,
        perfil: false,
        historico: false,
        partidas: true,
        stats: false,
        partidaRapida: false,
      },
      formOptions: PAISES,
    };
  },
  methods: {
    hazVisible(prop) {
      for (let attr in this.visibilidad) {
        if (attr === prop) {
          this.visibilidad[attr] = true;
        } else {
          this.visibilidad[attr] = false;
        }
      }
    },
    updatePerfil(d) {
      this.datosNuevosusuario = d;
    },
    updateAvatar(d) {
      this.nuevaRutaAvatar = d;
    },
    updateRetos(d) {
      this.nuevosChallenges = d;
    },
    updateNotificaciones(d) {
      this.cuantasNotificaciones = d;
    },
    logout() {
      return axios.post("/scrabble/logout").then(() => {
        window.location.href = "/scrabble/";
      });
    },
  },
});
