@extends('layouts.plantilla-estatica',
    ['titulo' => 'Reinicio de Contraseña'])
@section('contenido')
    <div class="rejilla">
    <div class="caja-formulario">
        <h3 class="titulos"> Reinicio de contraseña </h3>
        @if ($errors->isNotEmpty())
        <p>Corrige los siguientes campos</p>
        @endif
        <form method="POST" action="{{ route('password.request') }}">
            @csrf <!-- por razones educativas está desactivado -->
            <input type="hidden" name="token" value="{{ $token }}">
            <label class="titulos" for="email">Correo electrónico</label>     
            @if ($errors->has('email'))
                    <p>{{ $errors->first('email') }}</p>
                @endif
            <input id="email" type="email" name="email" value="{{ $email ?? old('email')}}"  class="{{ $errors->has('email') ? 'error' : '' }}" required autofocus>
            <label class="titulos" for="password">Contraseña</label>
            @if ($errors->has('password'))
                    <p>{{ $errors->first('password') }}</p>
            @endif
            <input id="password" type="password" name="password" pattern="^\S{5,}$" title="Al menos 5 Caracteres" required class="{{ $errors->has('password') ? 'error' : '' }}">
            <label  class="titulos" for="password-confirm">Confirma contraseña</label>
            <input id="password-confirm" type="password" pattern="^\S{5,}$" title="Al menos 5 Caracteres" name="password_confirmation" class="{{ $errors->has('password') ? 'error' : '' }}" required>
            <input class ="btn btn-dark" type="submit" value="Reinicia Contraseña">
        </form>
    </div>
</div>

@endsection
